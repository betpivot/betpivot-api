package com.betting.api;

import com.betting.storage.data.betting.BettingProviderBet;
import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.util.MachineLearningStatisticalServiceImpl;
import com.betting.storage.util.StatisticalServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MachineLearningStatisticalServiceImpl.class)
public class MachineLearningStatisticalServiceTests {

    @Autowired
    private MachineLearningStatisticalServiceImpl statisticalService;

    @Test
    public void serviceNotNull() {
        assertThat(statisticalService).isNotNull();
    }

    @Test
    public void test() {
        Prediction prediction = new Prediction(null, 0.85, 40.0, 11.5, null);
        BettingProviderBet bet = new BettingProviderBet(null, 8.5, null, null, null, null, null);
        double probability = statisticalService.getProbability(prediction, bet);


        assertThat(new BigDecimal(probability).setScale(10, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(0.671097).setScale(10, RoundingMode.HALF_UP));
    }
}
