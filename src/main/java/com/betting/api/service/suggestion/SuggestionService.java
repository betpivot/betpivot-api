package com.betting.api.service.suggestion;

import com.betting.api.model.init.RestBettingProvider;
import com.betting.api.model.suggestion.RestSuggestionMatch;
import com.betting.api.service.Evictable;

import java.util.List;

public interface SuggestionService extends Evictable {

    List<RestBettingProvider> getActiveBettingProviders();
    List<RestSuggestionMatch> getActiveSuggestions(List<String> asList);
}
