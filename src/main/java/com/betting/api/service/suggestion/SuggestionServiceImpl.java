package com.betting.api.service.suggestion;

import com.betting.api.model.init.RestBettingProvider;
import com.betting.api.model.suggestion.*;
import com.betting.api.rest.controller.MediaController;
import com.betting.api.util.ApiCountry;
import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.monitoring.Monitoring;
import com.betting.api.util.MatchService;
import com.betting.storage.data.suggestion.Suggestion;
import com.betting.storage.data.suggestion.SuggestionMatch;
import com.betting.storage.data.suggestion.SuggestionPlayer;
import com.betting.storage.data.suggestion.SuggestionState;
import com.betting.storage.service.betting.BettingProviderStorageService;
import com.betting.storage.service.push.PushNotificationType;
import com.betting.storage.service.suggestion.SuggestionStorageService;
import com.betting.storage.util.HashingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
@CacheConfig(cacheNames = { "suggestion" } )
public class SuggestionServiceImpl implements SuggestionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SuggestionServiceImpl.class);

    private final BettingProviderStorageService bettingStorage;
    private final HashingService hashingService;
    private final MatchService matchService;
    private final SuggestionStorageService suggestionStorage;

    @Autowired
    public SuggestionServiceImpl(BettingProviderStorageService bettingStorage, HashingService hashingService, MatchService matchService, SuggestionStorageService suggestionStorage) {
        this.bettingStorage = bettingStorage;
        this.hashingService = hashingService;
        this.matchService = matchService;
        this.suggestionStorage = suggestionStorage;
    }

    @Override
    @Cacheable
    public List<RestBettingProvider> getActiveBettingProviders() {
        LOGGER.info("Calling getActiveBettingProviders()");
        return bettingStorage.getActiveProviders().stream()
                .map(this::processOneBettingProvider)
                .collect(Collectors.toList());
    }

    private RestBettingProvider processOneBettingProvider(BettingProvider p) {
        //TODO COUNTRY
        String iconLink = ControllerLinkBuilder.linkTo(methodOn(MediaController.class).getImage("cz", p.getIcon(), null)).toUri().toString();
        String logoLink = ControllerLinkBuilder.linkTo(methodOn(MediaController.class).getImage("cz", p.getLogo(), null)).toUri().toString();
        String providerId = hashingService.hash(p.getId().toHexString());
        String topic = PushNotificationType.BETS_FIRST_LOAD.getTopicWithSuffix(providerId);
        return new RestBettingProvider(providerId, p.getName(), logoLink, iconLink, p.getColor(), topic, p.getRegistrationUrl());
    }

    @Override
    @Cacheable
    public List<RestSuggestionMatch> getActiveSuggestions(List<String> asList) {
        LOGGER.info("Calling getActiveSuggestions()");
        return suggestionStorage.getActiveMatches().stream()
                .map(this::processOnePredictionMatch)
                .sorted(Comparator.comparing(sm -> sm.startTime))
                .collect(Collectors.toList());
    }

    @Override
    @CacheEvict(allEntries=true)
    public void evict() {
        Monitoring.serviceInfo("Evicting cache", "api.suggestion.cache.evictClientPromo", null, getClass());
    }

    private RestSuggestionMatch processOnePredictionMatch(SuggestionMatch match) {
        ZonedDateTime matchStart = match.getStartTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        if (matchService.isFinished(match.getStartTime())) {
            RestSuggestionTeam team1 = new RestSuggestionTeam(match.getTeam1().getNameId(), null);
            RestSuggestionTeam team2 = new RestSuggestionTeam(match.getTeam2().getNameId(), null);
            return new RestSuggestionMatch(matchStart, RestSuggestionMatchState.FINISHED, team1, team2);
        }
        if (matchService.isPlaying(match.getStartTime())) {
            RestSuggestionTeam team1 = new RestSuggestionTeam(match.getTeam1().getNameId(), null);
            RestSuggestionTeam team2 = new RestSuggestionTeam(match.getTeam2().getNameId(), null);
            return new RestSuggestionMatch(matchStart, RestSuggestionMatchState.PLAYING, team1, team2);
        }

        List<RestSuggestionPlayer> restPlayers1 = match.getTeam1().getPlayers().stream()
                .map(this::processOnePlayer)
                .collect(Collectors.toList());

        List<RestSuggestionPlayer> restPlayers2 = match.getTeam2().getPlayers().stream()
                .map(this::processOnePlayer)
                .collect(Collectors.toList());

        List<RestSuggestionState> states = Stream.concat(restPlayers1.stream(), restPlayers2.stream())
                .flatMap(p -> p.suggestions.stream().map(s -> s.state))
                .collect(Collectors.toList());
        RestSuggestionMatchState state = matchService.resolveSuggestionState(match.getStartTime(), states);

        RestSuggestionTeam team1;
        RestSuggestionTeam team2;
        if (RestSuggestionMatchState.WAITING.equals(state)) {
            team1 = new RestSuggestionTeam(match.getTeam1().getNameId(), Collections.emptyList());
            team2 = new RestSuggestionTeam(match.getTeam2().getNameId(), Collections.emptyList());
        } else {
            team1 = new RestSuggestionTeam(match.getTeam1().getNameId(), restPlayers1);
            team2 = new RestSuggestionTeam(match.getTeam2().getNameId(), restPlayers2);
        }

        return new RestSuggestionMatch(matchStart, state, team1, team2);
    }

    private RestSuggestionPlayer processOnePlayer(SuggestionPlayer player) {
        Comparator<RestSuggestion> probComp = Comparator.comparing(r -> r.probability);
        List<RestSuggestion> restSuggestions = player.getSuggestions().stream()
                .map(this::processOneBettingProvider)
                .sorted(probComp.reversed())
                .collect(Collectors.toList());
        String suggestionToPredictionId = player.getExternalId();
        return new RestSuggestionPlayer(player.getPlayerName(), suggestionToPredictionId, restSuggestions);
    }

    private RestSuggestion processOneBettingProvider(Suggestion suggestion) {
        String providerId = suggestion.getProviderId();
        RestSuggestionState state = RestSuggestionState.valueOf(SuggestionState.WILL_NOT_PLAY.equals(suggestion.getState()) ? SuggestionState.WAITING_FOR_PROVIDER.name() : suggestion.getState().name());
        Double probability = suggestion.getProbability() == null ? 0.0 : suggestion.getProbability();
        RestSuggestionType type = suggestion.getType() == null ? null : RestSuggestionType.valueOf(suggestion.getType().name());
        Double line = suggestion.getLine();
        Double dontBetUnder = suggestion.getDontBetUnder();
        Double rate = suggestion.getRate();
        String betLink = suggestion.getBetLink();
        String betDeepLink = suggestion.getBetDeepLink();
        return new RestSuggestion(providerId, type, dontBetUnder, rate, line, betLink, betDeepLink, state, probability);
    }

}
