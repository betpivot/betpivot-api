package com.betting.api.service.promotion.performance;

import com.betting.api.model.promotion.RestClientPromotion;
import com.betting.api.model.promotion.RestClientPromotionState;
import com.betting.api.model.promotion.RestPromotion;
import com.betting.api.model.promotion.RestPromotionState;
import com.betting.api.util.ApiCountry;
import com.betting.storage.data.promotion.ClientPromotion;
import com.betting.storage.data.promotion.ClientPromotionUsage;
import com.betting.storage.data.promotion.Promotion;
import com.betting.storage.fail.ClientMessage;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.service.promotion.PromotionStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@CacheConfig
public class PromotionServiceImpl implements PromotionService {

    private final Map<String, List<LocalDateTime>> activationAttempts = new HashMap<>();

    private final PromotionStorageService promotionStorage;

    @Autowired
    public PromotionServiceImpl(PromotionStorageService promotionStorage) {
        this.promotionStorage = promotionStorage;
    }

    @Override
    @Cacheable(cacheNames = { "promotions-validate"})
    public RestPromotion validatePromoCode(String promoCodeValue) {
        return promotionStorage
                .getActivePromotion(promoCodeValue, LocalDateTime.now(ZoneOffset.UTC))
                .map(this::processOnePromotion)
                .orElseThrow(() -> ClientMessage.WRONG_PROMO_CODE.exception("Promo code is not valid."));
    }

    private RestPromotion processOnePromotion(Promotion p) {
        ZonedDateTime start = p.getStart().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        ZonedDateTime end = p.getEnd().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        RestPromotionState state = RestPromotionState.valueOf(p.getState().name());
        return new RestPromotion(start, end, state);
    }

    @Override
    @Cacheable(cacheNames = { "promotions-all-active"})
    public List<RestPromotion> getAllActivePromotions() {
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        return promotionStorage.getAllActivePromotions(now).stream()
                .map(this::processOnePromotion).collect(Collectors.toList());
    }

    @Override
    @Cacheable(cacheNames = { "promotions-client-id"})
    public RestClientPromotion getOrCreateClientPromotion(String clientId) {
        ClientPromotion promo = promotionStorage.getClientPromotionByClientId(clientId).orElseGet(() -> {
            ClientPromotion newPromo = new ClientPromotion(clientId);
            promotionStorage.save(newPromo);
            return newPromo;
        });
        return processOneClientPromotion(promo);
    }

    private RestClientPromotion processOneClientPromotion(ClientPromotion promo) {
        ZonedDateTime start = promo.getStart() == null ? null : promo.getStart().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        ZonedDateTime end = promo.getEnd() == null ? null : promo.getEnd().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        RestClientPromotionState state;
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        if (start != null && end != null && promo.getStart().isBefore(now) && promo.getEnd().isAfter(now)) {
            state = RestClientPromotionState.ACTIVE;
        } else {
            state = RestClientPromotionState.INACTIVE;
        }


        return new RestClientPromotion(start, end, state, promo.getPromoCode(), promo.getClientId(), promo.getUsedBy().size());
    }

    @Override
    public RestClientPromotion activateClientPromotion(String clientId, String promoCodeValue) {
        String lock = clientId.intern();
        synchronized (lock) {
            LocalDateTime tenSecondsAgo = LocalDateTime.now(ZoneOffset.UTC).minus(10, ChronoUnit.SECONDS);
            activationAttempts.putIfAbsent(clientId, new ArrayList<>());
            List<LocalDateTime> attempts = activationAttempts.get(clientId);
            List<LocalDateTime> currentAttempts = attempts.stream()
                    .filter(a -> a.isAfter(tenSecondsAgo))
                    .collect(Collectors.toList());
            long numberOfAttemptsInTenSeconds = currentAttempts.size();
            if (numberOfAttemptsInTenSeconds >= 3) {
                throw ClientMessage.TOO_MANY_ACTIVATION_ATTEMPTS.exception("You can try to activate only 3 times in 10 seconds.");
            }
            currentAttempts.add(LocalDateTime.now(ZoneOffset.UTC));
            activationAttempts.put(clientId, currentAttempts);

            promotionStorage.getClientPromotionUsage(clientId)
                    .ifPresent(c -> { throw ClientMessage.CLIENT_ALREADY_ACTIVATED.exception("Each client can activate promo code only once."); });
            ClientPromotion promo = promotionStorage.getClientPromotionByCode(promoCodeValue)
                    .orElseThrow(() -> ClientMessage.WRONG_PROMO_CODE.exception("Promo code is not valid."));

            if (promo.getClientId().equalsIgnoreCase(clientId)) {
                throw ClientMessage.CANNOT_ACTIVATE_OWN_PROMO_CODE.exception("Activating own promocode is obviously not allowed.");
            }

            promotionStorage.getClientPromotionByClientId(clientId)
                    .filter(p -> p.usedByClientId(promo.getClientId()))
                    .ifPresent(p -> { throw ClientMessage.CANNOT_CREATE_BILATERAL_RELATIONSHIP_BETWEEN_PROMO_CODES.exception("Cannot activate promo code of a client that has already activated promo code for the activation client"); });

            ClientPromotionUsage usage = new ClientPromotionUsage(clientId);

            promo.getUsedBy().add(usage);

            if (promo.getStart() == null || promo.getEnd().isBefore(LocalDateTime.now(ZoneOffset.UTC))) {
                promo.setStart(LocalDateTime.now(ZoneOffset.UTC));
                promo.setEnd(promo.getStart().plus(1, ChronoUnit.WEEKS));
            } else {
                promo.setEnd(promo.getEnd().plus(1, ChronoUnit.WEEKS));
            }

            promotionStorage.save(usage);
            promotionStorage.save(promo);
            return processOneClientPromotion(promo);
        }
    }

    @Override
    @CacheEvict(cacheNames = { "promotions-client-id" })
    public void evictClientPromo(String clientId) {
    }

    @Override
    @CacheEvict(allEntries=true, cacheNames = { "performance-result", "performance-period", "promotions-client-id" })
    public void evict() {
        Monitoring.serviceInfo("Evicting cache", "api.promotion.cache.evictClientPromo", null, getClass());
    }
}
