package com.betting.api.service.promotion.performance;

import com.betting.api.model.promotion.RestClientPromotion;
import com.betting.api.model.promotion.RestPromotion;
import com.betting.api.service.Evictable;
import org.springframework.cache.annotation.CacheEvict;

import java.util.List;

public interface PromotionService extends Evictable {

    RestPromotion validatePromoCode(String promoCodeValue);

    List<RestPromotion> getAllActivePromotions();

    RestClientPromotion getOrCreateClientPromotion(String clientId);

    RestClientPromotion activateClientPromotion(String clientId, String promoCodeValue);

    @CacheEvict(cacheNames = { "promotions-client-id" }, key = "#clientId")
    void evictClientPromo(String clientId);
}
