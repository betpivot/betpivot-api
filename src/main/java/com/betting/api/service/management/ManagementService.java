package com.betting.api.service.management;

import com.betting.api.model.management.*;
import com.betting.storage.service.push.PushNotificationType;

import java.util.List;

public interface ManagementService {
    RestAddMatchResult addMatch(String body);

    RestEvaluateMatchResult evaluateMatch(String body);

    RestAddBetsResult addDebugBets(List<RestAddBetsMatch> body);

    RestAddPerformanceResult addDebugPerformance(String body);

    void notification(PushNotificationType type, String providerName);

    RestInvalidateCacheResponse invalidateCache(String cacheName);
}
