package com.betting.api.service.management;

import com.betting.api.model.management.*;
import com.betting.storage.data.messaging.BettingMessage;
import com.betting.storage.data.messaging.BettingMessageType;
import com.betting.storage.data.performance.PerformanceResult;
import com.betting.storage.data.performance.PerformanceResultValue;
import com.betting.storage.data.suggestion.SuggestionType;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.fail.BusinessFail;
import com.betting.api.service.performance.PerformanceService;
import com.betting.api.service.prediction.PredictionService;
import com.betting.api.service.suggestion.SuggestionService;
import com.betting.storage.data.betting.*;
import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.data.prediction.csv.PredictionCsvFile;
import com.betting.storage.data.prediction.csv.ResultsCsvFile;
import com.betting.storage.data.prediction.csv.ResultsCsvFileLine;
import com.betting.storage.data.result.PredictionResult;
import com.betting.storage.service.betting.BettingProviderStorageService;
import com.betting.storage.service.messaging.MessagingStorageService;
import com.betting.storage.service.performance.PerformanceIntegrationService;
import com.betting.storage.service.prediction.PredictionStorageService;
import com.betting.storage.service.push.PushNotificationService;
import com.betting.storage.service.push.PushNotificationType;
import com.betting.storage.service.result.ResultStorageService;
import com.betting.storage.service.suggestion.SuggestionIntegrationService;
import com.betting.storage.service.suggestion.SuggestionStorageService;
import com.betting.storage.util.HashingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class ManagementServiceImpl implements ManagementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagementServiceImpl.class);

    private final PredictionStorageService predictionStorage;
    private final SuggestionService suggestionService;
    private final PredictionService predictionService;
    private final PerformanceService performanceService;
    private final BettingProviderStorageService bettingStorageService;
    private final SuggestionIntegrationService suggestionIntegrationService;
    private final PerformanceIntegrationService performanceIntegrationService;
    private final SuggestionStorageService suggestionStorage;
    private final ResultStorageService resultStorage;
    private final HashingService hashingService;
    private final PushNotificationService pushService;
    private final MessagingStorageService messagingStorage;

    @Autowired
    public ManagementServiceImpl(PredictionStorageService predictionStorage, SuggestionService suggestionService, PredictionService predictionService, PerformanceService performanceService, BettingProviderStorageService bettingStorageService, SuggestionIntegrationService suggestionIntegrationService, PerformanceIntegrationService performanceIntegrationService, SuggestionStorageService suggestionStorage, ResultStorageService resultStorage, HashingService hashingService, PushNotificationService pushService, MessagingStorageService messagingStorage) {
        this.predictionStorage = predictionStorage;
        this.suggestionService = suggestionService;
        this.predictionService = predictionService;
        this.performanceService = performanceService;
        this.bettingStorageService = bettingStorageService;
        this.suggestionIntegrationService = suggestionIntegrationService;
        this.performanceIntegrationService = performanceIntegrationService;
        this.suggestionStorage = suggestionStorage;
        this.resultStorage = resultStorage;
        this.hashingService = hashingService;
        this.pushService = pushService;
        this.messagingStorage = messagingStorage;
    }

    @Override
    public RestAddMatchResult addMatch(String body) {
        LOGGER.info("Adding matches with data:\n{}", body);
        PredictionCsvFile file = new PredictionCsvFile(predictionStorage, hashingService, body);
        List<PredictionMatch> dbData = file.toDbData();
        long predictionMatchesWithSameTeamsButDifferentStart = predictionStorage.getActivePredictions().stream()
                .filter(m -> dbData.stream()
                        .anyMatch(
                                dbm -> dbm.getTeam1().getNameId().equals(m.getTeam1().getNameId()) &&
                                        dbm.getTeam2().getNameId().equals(m.getTeam2().getNameId()) &&
                                        !dbm.getStartTime().equals(m.getStartTime()))).count();

        if (predictionMatchesWithSameTeamsButDifferentStart > 0) {
            throw BusinessFail.ANOTHER_RUNNING_PREDICTION_MATCH
                    .exception(Monitoring.BusinessObject.PREDICTION, "Cannot import match if there is ACTIVE match with same teams and different start time. Evaluate match first and try to import again.", file.toString(), getClass());
        }

        long oldMatches = predictionStorage.saveMatches(dbData);

        suggestionIntegrationService.calculateSuggestions();
        suggestionService.evict();
        predictionService.evict();

        boolean sendNotification = oldMatches == 0;
        if (sendNotification) {
            pushService.notifyNewPredictions();
        }
        messagingStorage.postMessage(new BettingMessage(BettingMessageType.NEW_PREDICTION, "API"));
        return new RestAddMatchResult(dbData.size(), Math.toIntExact(dbData.stream().mapToLong(m -> m.getAllPredictions().size()).sum()), Math.toIntExact(oldMatches), sendNotification);
    }

    @Override
    public RestEvaluateMatchResult evaluateMatch(String body) {
        LOGGER.info("Evaluating matches with data:\n{}", body);

        ResultsCsvFile file = new ResultsCsvFile(body);
        LOGGER.debug("Processing CSV");
        Map<String, List<PredictionResult>> results = file.lines.stream()
                .map(this::processOneResult)
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.groupingBy(PredictionResult::getExternalMatchId));
        LOGGER.debug("Updating prediction matches");
        long evaluatedPredictions = predictionStorage.evaluateAll(results.keySet());
        LOGGER.debug("Updating suggestion matches");
        long evaluatedSuggestions = suggestionStorage.evaluateAll(results.keySet());

        suggestionIntegrationService.calculateSuggestions();
        performanceIntegrationService.recalculatePerformance(results);
        resultStorage.saveResult(results);

        LOGGER.debug("Evicting caches");
        predictionService.evict();
        suggestionService.evict();
        performanceService.evict();

        boolean sendNotification = evaluatedPredictions > 0;
        if (sendNotification) {
            LOGGER.debug("Firebase push");
            pushService.notifyNewResults();
        }

        Integer resultCount = results.entrySet().stream().map(r -> r.getValue().size()).reduce((c1, c2) -> c1 + c2).orElse(0);
        RestEvaluateMatchResult restEvaluateMatchResult = new RestEvaluateMatchResult(evaluatedPredictions, evaluatedSuggestions, resultCount, sendNotification);
        LOGGER.info(restEvaluateMatchResult.toString());
        return restEvaluateMatchResult;
    }

    private Optional<PredictionResult> processOneResult(ResultsCsvFileLine l) {
        if (l.isFirst() || "DEVEL".equals(l.column2)) {
            return Optional.empty();
        }
        String externalId = hashingService.hash(l.column1);
        String externalMatchId = hashingService.hash(l.column8);
        try {
            int points = Integer.parseInt(l.column5);
            return Optional.of(new PredictionResult(points, true, externalId, externalMatchId));
        } catch (NumberFormatException e) {
            return Optional.of(new PredictionResult(null, false, externalId, externalMatchId));
        }
    }

    @Override
    public RestAddBetsResult addDebugBets(List<RestAddBetsMatch> body) {
        Map<BettingProvider, List<BettingProviderMatch>> providerMatches = body.stream().map(m -> {
            List<BettingProviderBet> bets = Stream.concat(m.getTeam1Bets().stream(), m.getTeam2Bets().stream())
                    .map(s -> {
                        String firstName = s.getPlayerName().getFirstName();
                        String lastName = s.getPlayerName().getLastName();
                        BettingProviderBetType type = BettingProviderBetType.valueOf(s.getType().name());
                        return new BettingProviderBet(new BettingProviderPlayerName(firstName, lastName), s.getLine(), type, s.getRate(), s.getBetLinkUrl(), null, null);
                    }).collect(Collectors.toList());
            return bettingStorageService.getActiveProvider(m.getProviderName()).map(bp -> {
                BettingProviderMatch match = new BettingProviderMatch(m.getTeam1(), m.getTeam2(), bp, m.getExternalMatchId());
                match.getBets().addAll(bets);
                return match;
            }).orElseThrow(() -> BusinessFail.PROVIDER_NOT_FOUND.exception(Monitoring.BusinessObject.BET, "Cannot add debug bets. Check provider name and try again.", m.getProviderName(), getClass()));

        }).collect(Collectors.groupingBy(BettingProviderMatch::getProvider));

        providerMatches.forEach((key, value) -> bettingStorageService.saveData(value, key));

        suggestionIntegrationService.calculateSuggestions();
        suggestionService.evict();
        predictionService.evict();
        performanceService.evict();
        long numberOfBets = providerMatches.values().stream().flatMap(m -> m.stream().map(mm -> mm.getBets().stream())).count();
        long numberOfMatches = providerMatches.values().stream().mapToLong(Collection::size).sum();
        return new RestAddBetsResult(numberOfBets, numberOfMatches);
    }

    @Override
    public RestAddPerformanceResult addDebugPerformance(String body) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");
        List<PerformanceResult> results = Arrays.stream(body.split("([\r\n]+)"))
                .skip(1)
                .map(l -> {
                    String[] columns = l.replace("\"", "").split(",");
                    String teamNameId = predictionStorage.getTeam(columns[8])
                            .orElseThrow(() -> BusinessFail.TEAM_NOT_FOUND.exception(Monitoring.BusinessObject.RESULT, "Cannot import peformance", columns[8], getClass()))
                            .getNameId();
                    String providerId = hashingService
                            .hash(bettingStorageService
                                    .getActiveProvider(columns[4])
                                    .orElseThrow(() -> BusinessFail.PROVIDER_NOT_FOUND.exception(Monitoring.BusinessObject.RESULT, "Cannot import peformance", columns[4], getClass()))
                                    .getId().toHexString());
                    SuggestionType type = "POD".equals(columns[1]) ? SuggestionType.LESS : SuggestionType.MORE;
                    Double rate = Double.valueOf(columns[5]);
                    Double line = Double.valueOf(columns[2]);
                    Integer result = Integer.valueOf(columns[3]);
                    LocalDateTime betImportTime = LocalDateTime.parse(columns[7], format).atZone(ZoneOffset.UTC).toLocalDateTime();
                    PerformanceResultValue resultValue = "1".equals(columns[6]) ? PerformanceResultValue.WINNING : PerformanceResultValue.NOT_WINNING;
                    return new PerformanceResult(columns[0], teamNameId, providerId, type, 0.0, rate, line, result, betImportTime, LocalDateTime.now(ZoneOffset.UTC), resultValue, 0.0, columns[9], columns[10]);
                }).collect(Collectors.toList());

        performanceIntegrationService.importPerformance(results);

        return new RestAddPerformanceResult(results.size());
    }

    @Override
    public void notification(PushNotificationType type, String providerName) {
        switch (type) {
            case PREDICTION_NEW: pushService.notifyNewPredictions();
                break;
            case BETS_FIRST_LOAD: {
                BettingProvider bp = bettingStorageService.getActiveProvider(providerName)
                        .orElseThrow(() -> BusinessFail.PROVIDER_NOT_FOUND.exception(Monitoring.BusinessObject.BET, "Cannot send notification", providerName, getClass()));
                pushService.notifyNewBets(bp);
            }
                break;
            case RESULTS_NEW: pushService.notifyNewResults();
                break;
        }
    }

    @Override
    public RestInvalidateCacheResponse invalidateCache(String cacheName) {
        StringBuilder response = new StringBuilder();
        if (SuggestionService.class.getSimpleName().equalsIgnoreCase(cacheName)) {
            suggestionService.evict();
            response.append("suggestionService evicted");
        }
        if (PerformanceService.class.getSimpleName().equalsIgnoreCase(cacheName)) {
            performanceService.evict();
            response.append("performanceService evicted");
        }
        if (PredictionService.class.getSimpleName().equalsIgnoreCase(cacheName)) {
            predictionService.evict();
            response.append("predictionService evicted");
        }
        return new RestInvalidateCacheResponse(response.append(" done").toString());
    }
}
