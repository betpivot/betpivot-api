package com.betting.api.service.result;

import com.betting.api.model.result.RestPredictionResult;

import java.util.List;
import java.util.Set;

public interface ResultService {

    List<RestPredictionResult> getByExternalIds(Set<String> externalIds);
}
