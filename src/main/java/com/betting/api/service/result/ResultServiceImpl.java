package com.betting.api.service.result;

import com.betting.api.model.result.RestPredictionResult;
import com.betting.api.util.ApiCountry;
import com.betting.storage.data.result.PredictionResult;
import com.betting.storage.service.result.ResultStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ResultServiceImpl implements ResultService {

    private final ResultStorageService resultStorage;

    @Autowired
    public ResultServiceImpl(ResultStorageService resultStorage) {
        this.resultStorage = resultStorage;
    }

    @Override
    public List<RestPredictionResult> getByExternalIds(Set<String> externalIds) {
        if (externalIds == null || externalIds.isEmpty()) {
            return Collections.emptyList();
        }
        return resultStorage.getByExternalIds(externalIds).stream()
                .map(this::processOneResult).collect(Collectors.toList());
    }

    private RestPredictionResult processOneResult(PredictionResult r) {
        ZonedDateTime importTime = r.getImportTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        return new RestPredictionResult(r.getPoints(), r.getPlayed(), r.getExternalId(), importTime);
    }
}
