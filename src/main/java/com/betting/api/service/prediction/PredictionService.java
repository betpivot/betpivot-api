package com.betting.api.service.prediction;

import com.betting.api.model.prediction.RestPredictionMatch;
import com.betting.api.service.Evictable;

import java.util.List;

public interface PredictionService extends Evictable {
     List<RestPredictionMatch> getPredictions();
}
