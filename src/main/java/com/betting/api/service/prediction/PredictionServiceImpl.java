package com.betting.api.service.prediction;

import com.betting.api.model.prediction.*;
import com.betting.api.util.ApiCountry;
import com.betting.storage.data.suggestion.Suggestion;
import com.betting.storage.data.suggestion.SuggestionMatch;
import com.betting.storage.data.suggestion.SuggestionMatchState;
import com.betting.storage.monitoring.Monitoring;
import com.betting.api.util.MatchService;
import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.service.prediction.PredictionStorageService;
import com.betting.storage.service.suggestion.SuggestionStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = { "prediction" } )
public class PredictionServiceImpl implements PredictionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredictionServiceImpl.class);

    @Autowired
    private PredictionStorageService predictionStorage;

    @Autowired
    private SuggestionStorageService suggestionStorage;

    @Autowired
    private MatchService matchService;

    @Override
    @Cacheable
    public List<RestPredictionMatch> getPredictions() {
        LOGGER.info("Calling getPredictions()");
        Map<String, List<SuggestionMatch>> suggestionMatches = suggestionStorage.getActiveMatches().stream()
                .collect(Collectors.groupingBy(SuggestionMatch::getExternalMatchId));

        return predictionStorage.getActivePredictions().stream()
                .map(m -> {
                    List<SuggestionMatch> suggestionMatchesForPrediction = suggestionMatches.get(m.getExternalMatchId());
                    return processOneMatch(m, suggestionMatchesForPrediction);
                })
                .sorted(Comparator.comparing(pm -> pm.startTime))
                .collect(Collectors.toList());
    }

    @Override
    @CacheEvict(allEntries=true)
    public void evict() {
        Monitoring.serviceInfo("Evicting cache", "api.prediction.cache.evictClientPromo", null, getClass());
    }

    private RestPredictionMatch processOneMatch(PredictionMatch predictionMatch, List<SuggestionMatch> suggestionMatches) {

        ZonedDateTime matchStart = predictionMatch.getStartTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        if (matchService.isFinished(predictionMatch.getStartTime())) {
            RestPredictionTeam team1 = new RestPredictionTeam(predictionMatch.getTeam1().getNameId(), Collections.emptyList());
            RestPredictionTeam team2 = new RestPredictionTeam(predictionMatch.getTeam2().getNameId(), Collections.emptyList());
            return new RestPredictionMatch(matchStart, RestPredictionMatchState.FINISHED, team1, team2);
        }
        if (matchService.isPlaying(predictionMatch.getStartTime())) {
            RestPredictionTeam team1 = new RestPredictionTeam(predictionMatch.getTeam1().getNameId(), Collections.emptyList());
            RestPredictionTeam team2 = new RestPredictionTeam(predictionMatch.getTeam2().getNameId(), Collections.emptyList());
            return new RestPredictionMatch(matchStart, RestPredictionMatchState.PLAYING, team1, team2);
        }

        List<RestPredictionPlayer> restPlayers1 = predictionMatch.getTeam1Predictions().stream()
                .map(p -> processOnePlayer(p, suggestionMatches))
                .collect(Collectors.toList());
        List<RestPredictionPlayer> restPlayers2 = predictionMatch.getTeam2Predictions().stream()
                .map(p -> processOnePlayer(p, suggestionMatches))
                .collect(Collectors.toList());

        RestPredictionMatchState state = matchService.resolvePredictionState(predictionMatch.getStartTime());
        RestPredictionTeam team1 = new RestPredictionTeam(predictionMatch.getTeam1().getNameId(), restPlayers1);
        RestPredictionTeam team2 = new RestPredictionTeam(predictionMatch.getTeam2().getNameId(), restPlayers2);
        return new RestPredictionMatch(matchStart, state, team1, team2);
    }

    private RestPredictionPlayer processOnePlayer(Prediction predictionSuggestion, List<SuggestionMatch> suggestionMatches) {
        String externalId = predictionSuggestion.getExternalId();
        Optional<SuggestionMatch> active;
        if (suggestionMatches != null) {
            List<SuggestionMatch> list = suggestionMatches.stream().filter(m -> m.getPlayerByExternalId(externalId).isPresent()).collect(Collectors.toList());
            if (list.size() == 0) {
                active = Optional.empty();
            } else {
                if (list.size() > 1) {
                    LOGGER.error("More then one active suggestion matches");
                }
                active = Optional.of(list.get(0));
            }
        } else {
            LOGGER.error("Cannot find suggestion match when calculating {}", predictionSuggestion);
            active = Optional.empty();
        }
        PredictionToSuggestionState suggestionState = active
                .flatMap(m -> m.getPlayerByExternalId(externalId).flatMap(sp -> sp.getSuggestions().stream().map(Suggestion::getState).min(Comparator.comparing(s -> s.order))))
                .map(PredictionToSuggestionState::resolve)
                .orElse(PredictionToSuggestionState.MISSING);

        String playerName = predictionSuggestion.getPlayer().getFullName();
        RestPrediction prediction = new RestPrediction(predictionSuggestion.getProbability(), predictionSuggestion.getPlayTime(), predictionSuggestion.getPoints());
        return new RestPredictionPlayer(playerName, externalId, suggestionState, prediction);
    }
}
