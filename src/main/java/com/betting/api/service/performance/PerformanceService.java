package com.betting.api.service.performance;

import com.betting.api.model.performance.RestPerformancePeriod;
import com.betting.api.model.performance.RestPerformanceResult;
import com.betting.api.model.performance.RestPerformanceResultList;
import com.betting.api.service.Evictable;

import java.util.List;

public interface PerformanceService extends Evictable {

    List<RestPerformancePeriod> getPeriods();

    RestPerformanceResultList getResults();

    List<RestPerformanceResult> getAllResults();
}
