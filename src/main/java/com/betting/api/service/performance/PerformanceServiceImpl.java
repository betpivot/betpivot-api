package com.betting.api.service.performance;

import com.betting.api.model.performance.*;
import com.betting.api.model.suggestion.RestSuggestionType;
import com.betting.api.util.ApiCountry;
import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.config.SeasonConfiguration;
import com.betting.storage.data.performance.PerformanceResult;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.service.betting.BettingProviderStorageService;
import com.betting.storage.service.configuration.ConfigurationStorageService;
import com.betting.storage.service.performance.PerformanceStorageService;
import com.betting.storage.service.suggestion.SuggestionStorageService;
import com.betting.storage.util.DateRange;
import com.betting.storage.util.MutableDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.StrictMath.abs;

@Service
@CacheConfig
public class PerformanceServiceImpl implements PerformanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceServiceImpl.class);

    private final PerformanceStorageService performanceStorage;
    private final ConfigurationStorageService configurationStorage;
    private final BettingProviderStorageService bettingStorage;

    @Autowired
    public PerformanceServiceImpl(PerformanceStorageService performanceStorage, ConfigurationStorageService configurationStorage, SuggestionStorageService suggestionStorage, BettingProviderStorageService bettingStorage) {
        this.performanceStorage = performanceStorage;
        this.configurationStorage = configurationStorage;
        this.bettingStorage = bettingStorage;
    }

    @Override
    @Cacheable(cacheNames = { "performance-period" } )
    public List<RestPerformancePeriod> getPeriods() {
        LOGGER.info("Called getPeriods()");
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        LocalDateTime seasonStart = configurationStorage.getSeason(now).map(SeasonConfiguration::getStart).orElse(null);
        return Stream.of(RestPerformancePeriodTitle.PERFORMANCE_ALL, RestPerformancePeriodTitle.PERFORMANCE_THIS_SEASON)
                .map(t -> processOnePeriod(t, now, seasonStart))
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(cacheNames = { "performance-result" } )
    public RestPerformanceResultList getResults() {
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        LocalDateTime seasonStart = configurationStorage.getSeason(now).map(SeasonConfiguration::getStart).orElse(null);
        Comparator<RestPerformanceResult> betImportTimeComparator = Comparator.comparing(RestPerformanceResult::getBetImportTime).reversed();
        List<RestPerformanceResult> results = getResults(RestPerformancePeriodTitle.PERFORMANCE_LAST_THREE_MONTH, seasonStart).stream().sorted(betImportTimeComparator).collect(Collectors.toList());
//        ZonedDateTime matchEned = ZonedDateTime.now(ZoneOffset.UTC)
//                .minus(2, ChronoUnit.HOURS)
//                .minus(20, ChronoUnit.MINUTES);
//        List<RestPerformanceResult> unEvaluatedResults = suggestionStorage.getActiveMatches().stream()
//                .filter(m -> m.getStartTime().isBefore(matchEned.toLocalDateTime()))
//                .flatMap(m -> Stream.concat(m.getTeam1().getPlayers().stream(), m.getTeam1().getPlayers().stream())
//                        .filter(p -> p.getSuggestions().stream().anyMatch(s -> SuggestionState.PROFITABLE.equals(s.getState())))
//                        .map(p -> {
//                            String teamNameId = m.belongsToTeam(p).map(SuggestionTeam::getNameId).orElse(null);
//                            ZonedDateTime betImportTime = m.getImportTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
//                            return new RestPerformanceResult(p.getPlayerName(), teamNameId, null, null, null, null, null, null, betImportTime, null, RestPerformanceResultValue.WAITING_FOR_EVALUATION);
//                        }))
//                .collect(Collectors.toList());
//        results.addAll(unEvaluatedResults);
        return new RestPerformanceResultList(results);
    }

    @Override
    @Cacheable(cacheNames = { "performance-all-result" } )
    public List<RestPerformanceResult> getAllResults() {
        Map<String, BettingProvider> providers = bettingStorage.getActiveProvidersMap();
        return performanceStorage.findAllActive().stream()
                .map(result -> processOneResult(providers, result))
                .collect(Collectors.toList());
    }

    private List<RestPerformanceResult> getResults(RestPerformancePeriodTitle title, LocalDateTime seasonStart) {
        LocalDateTime utcEnd = LocalDateTime.now(ZoneOffset.UTC);
        LocalDateTime utcStart = title.resolvePeriodStart(seasonStart);
        if (utcStart == null) {
            return Collections.emptyList();
        }
        Map<String, BettingProvider> providers = bettingStorage.getActiveProvidersMap();
        return performanceStorage.findActive(utcStart, utcEnd).stream()
                .map(result -> processOneResult(providers, result))
                .collect(Collectors.toList());
    }

    private RestPerformancePeriod processOnePeriod(RestPerformancePeriodTitle title, LocalDateTime now, LocalDateTime seasonStart) {
        LocalDateTime localStart = title.resolvePeriodStart(seasonStart);
        Comparator<RestPerformanceResult> betImportTimeComparator = Comparator.comparing(RestPerformanceResult::getBetImportTime);
        List<RestPerformanceResult> results = getResults(title, seasonStart).stream().sorted(betImportTimeComparator).collect(Collectors.toList());
        if (results.isEmpty()) {
            RestPerformanceGraph graph = new RestPerformanceGraph(0, 0,  Collections.emptyList());
            return new RestPerformancePeriod(title, null, null, null, null, 0, graph);

        }

        ZonedDateTime start = localStart.atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        ZonedDateTime end = now.atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        Double gain = calculateGain(results, RestPerformanceResult::getRate);
        Long cost = calculateCost(results);
        Double roiWithOptimalRate = cost == 0 ? 0.0 : (gain - cost) / cost;
        Double gainDontBetUnder = calculateGain(results, RestPerformanceResult::getDontBetUnder);
        Double roiWithDontBetUnderRate = cost == 0 ? 0.0 : (gainDontBetUnder - cost) / cost;
        int numberOfResults = results.size();
        RestPerformanceGraph graph = processGraph(results);

        return new RestPerformancePeriod(title, start, end, roiWithOptimalRate, roiWithDontBetUnderRate, numberOfResults, graph);
    }

    private static final int BRACKETS = 300;

    private RestPerformanceGraph processGraph(List<RestPerformanceResult> results) {
        List<List<RestPerformanceResult>> buckets = getBatches(results, roundUp(results.size(), BRACKETS));
        int numberOfBuckets = buckets.size();
        int pointsInBucket = buckets.size() > 0 ? buckets.get(0).size() : 0;
        MutableDouble accumulator = new MutableDouble(0.0);
        List<Double> dataPoints = buckets.stream().map(b -> {
            Double gain = calculateGain(b, RestPerformanceResult::getRate);
            Long cost = calculateCost(b);
            return accumulator.add(gain * 100 - cost * 100).val();
        }).collect(Collectors.toList());

        dataPoints.add(0, 0.0);
        return new RestPerformanceGraph(numberOfBuckets, pointsInBucket, dataPoints);
    }

    public static int roundUp(int num, int divisor) {
        int sign = (num > 0 ? 1 : -1) * (divisor > 0 ? 1 : -1);
        return sign * (abs(num) + abs(divisor) - 1) / abs(divisor);
    }

    public static <T> List<List<T>> getBatches(List<T> collection, int batchSize){
        int i = 0;
        List<List<T>> batches = new ArrayList<List<T>>();
        while(i<collection.size()){
            int nextInc = Math.min(collection.size()-i,batchSize);
            List<T> batch = collection.subList(i,i+nextInc);
            batches.add(batch);
            i = i + nextInc;
        }

        return batches;
    }

    private RestPerformanceTimeGraph processTimeGraph(LocalDateTime end, LocalDateTime start, List<RestPerformanceResult> results) {
        Map<LocalDate, List<Double>> dataPoints = results.stream()
                .collect(Collectors.groupingBy(r -> r.getBetImportTime().toLocalDate()))
                .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> {
                    Double optimalIncome = (calculateGain(e.getValue(), RestPerformanceResult::getRate) - calculateCost(e.getValue())) * 100;
                    Double dontBetUnderIncome = (calculateGain(e.getValue(), RestPerformanceResult::getDontBetUnder) - calculateCost(e.getValue())) * 100;
                    return Arrays.asList(optimalIncome, dontBetUnderIncome);
                }));
        List<LocalDate> dates = new DateRange(start.toLocalDate(), end.toLocalDate()).toList();
        List<List<Double>> resultDataPoints = new ArrayList<>();
        for (int i = 0; i < dates.size(); i++) {
            List<Double> dataPoint = dataPoints.get(dates.get(i)) == null ? Arrays.asList(0.0, 0.0) : dataPoints.get(dates.get(i));
            if (resultDataPoints.size() == 0) {
                resultDataPoints.add(dataPoint);
                continue;
            }
            List<Double> previousDataPoint = resultDataPoints.get(i - 1);
            resultDataPoints.add(Arrays.asList(previousDataPoint.get(0) + dataPoint.get(0), previousDataPoint.get(1) + dataPoint.get(1)));
        }

        return new RestPerformanceTimeGraph("PERFORMANCE_ROI_WITH_OPTIMAL_RATE", "PERFORMANCE_ROI_WITH_DONT_BET_UNDER_RATE", resultDataPoints);
    }

    private Double calculateGain(List<RestPerformanceResult> results, Function<RestPerformanceResult, Double> mapper) {
        return results.stream().filter(r -> RestPerformanceResultValue.WINNING.equals(r.getResultValue()))
                .map(mapper).reduce((r1, r2) -> r1 + r2).orElse(0.0);
    }

    private Long calculateCost(List<RestPerformanceResult> results) {
        return results.stream()
                .filter(r -> RestPerformanceResultValue.WINNING.equals(r.getResultValue()) || RestPerformanceResultValue.NOT_WINNING.equals(r.getResultValue()))
                .count();
    }

    private RestPerformanceResult processOneResult(Map<String, BettingProvider> providers, PerformanceResult result) {
        RestSuggestionType type = result.getType() == null ? null : RestSuggestionType.valueOf(result.getType().name());
        RestPerformanceResultValue resultValue = result.getResultValue() == null ? null :  RestPerformanceResultValue.valueOf(result.getResultValue().name());
        ZonedDateTime betImportTime = result.getBetImportTime() == null ? null : result.getBetImportTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        ZonedDateTime predictionEvaluationTime = result.getPredictionEvaluationTime() == null ? null : result.getPredictionEvaluationTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
        BettingProvider provider = providers.get(result.getProviderId());

        return new RestPerformanceResult(result.getPlayerName(), result.getTeamNameId(), result.getProviderId(), provider.getName(), type, result.getDontBetUnder(), result.getRate(), result.getLine(), result.getExternalId(), result.getResult(), betImportTime, predictionEvaluationTime, resultValue);
    }

    @Override
    @CacheEvict(allEntries=true, cacheNames = { "performance-result", "performance-period" })
    public void evict() {
        Monitoring.serviceInfo("Evicting cache", "api.performance.cache.evictClientPromo", null, getClass());
    }
}
