package com.betting.api.service.init;

import com.betting.api.model.init.*;
import com.betting.api.service.suggestion.SuggestionService;
import com.betting.api.util.ApiCountry;
import com.betting.storage.data.config.Configuration;
import com.betting.storage.data.config.SeasonConfiguration;
import com.betting.storage.fail.BusinessFail;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.service.configuration.ConfigurationStorageService;
import com.betting.storage.service.prediction.PredictionStorageService;
import com.betting.storage.util.HashingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = { "init" } )
public class InitServiceImpl implements InitService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InitServiceImpl.class);

    @Autowired
    private SuggestionService suggestionService;

    @Autowired
    private PredictionStorageService predictionStorage;

    @Autowired
    private ConfigurationStorageService configurationStorage;

    @Autowired
    private HashingService hashingService;
    @Override
    @Cacheable
    public RestInitResponse getInitConfig() {
        LOGGER.info("Called getInitConfig()");
        List<RestTeam> restTeams = predictionStorage.getTeams().stream().map(pt -> new RestTeam(pt.getNameId(), pt.getNames().get(1))).collect(Collectors.toList());
        List<RestBettingProvider> restProviders = suggestionService.getActiveBettingProviders();
        Configuration config = configurationStorage.getConfig()
                .orElseThrow(() -> BusinessFail.CONFIGURATION_MISSING
                        .exception(Monitoring.BusinessObject.INIT, "Configuration not found", null, getClass()));
        Optional<SeasonConfiguration> seasonConfiguration = configurationStorage.getSeason(LocalDateTime.now());
        RestSeason restSeason = seasonConfiguration.map(sc -> {
            LocalTime nextMatchStartTime = sc.getPredictionImportTime();
            ZonedDateTime nextMatch = LocalDate.now().atTime(nextMatchStartTime).atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
            if (nextMatchStartTime.isAfter(LocalTime.now())) {
                nextMatch = nextMatch.plus(1, ChronoUnit.DAYS);
            }
            String seasonId = hashingService.hash(sc.getSeasonId());
            ZonedDateTime start = sc.getStart().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
            ZonedDateTime end = sc.getEnd().atZone(ZoneOffset.UTC).withZoneSameInstant(ApiCountry.CZ.getZone());
            RestSeasonState state = RestSeasonState.valueOf(sc.getState().name());
            return new RestSeason(state, seasonId, start, end, nextMatch);
        }).orElseGet(() -> {
            Monitoring.serviceInfo("Season is not running", "api.init.season.notRunning", null, getClass());
            return new RestSeason(RestSeasonState.NOT_RUNNING, null, null, null, null);
        });
        if (config.getLocalization() == null) {
           throw BusinessFail.CONFIGURATION_MISSING
                    .exception(Monitoring.BusinessObject.INIT, "Localization config not found in configuration", config.toString(), getClass());
        }
        if (config.getUser() == null) {
            throw BusinessFail.CONFIGURATION_MISSING
                    .exception(Monitoring.BusinessObject.INIT, "User config not found in configuration", config.toString(), getClass());
        }

        String consentUrl = config.getUser().getPrivacyConsentUrl();
        String unlockAdUnitId = config.getUser().getUnlockAdUnitId();
        RestLocalization localization = new RestLocalization(config.getLocalization().getTeamJoinCharacter(), RestTeamOrder.valueOf(config.getLocalization().getTeamOrder().name()));
        return new RestInitResponse(ZonedDateTime.now(), restSeason, localization, consentUrl, unlockAdUnitId, restProviders, restTeams);
    }

    @Override
    @CacheEvict(allEntries=true)
    public void evict() {
        Monitoring.serviceInfo("Evicting cache", "api.init.cache.evictClientPromo", null, getClass());
    }
}
