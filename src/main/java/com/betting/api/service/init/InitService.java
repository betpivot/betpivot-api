package com.betting.api.service.init;

import com.betting.api.model.init.RestInitResponse;
import com.betting.api.service.Evictable;

public interface InitService extends Evictable {
    RestInitResponse getInitConfig();

}