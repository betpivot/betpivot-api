package com.betting.api.service;

public interface Evictable {

    void evict();
}
