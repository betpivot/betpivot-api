package com.betting.api.config;

import com.betting.storage.monitoring.Monitoring;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@WebFilter("/*")
public class StatsFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // empty
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        long time = System.currentTimeMillis();
        try {
            chain.doFilter(req, resp);
        } finally {
            time = System.currentTimeMillis() - time;
            Monitoring.serviceInfo(((HttpServletRequest) req).getRequestURI() + ": " + time + "ms", ((HttpServletRequest) req).getRequestURI(), String.valueOf(time), getClass());
        }
    }

    @Override
    public void destroy() {
        // empty
    }
}
