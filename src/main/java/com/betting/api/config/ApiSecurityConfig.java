package com.betting.api.config;

import com.betting.api.security.ApiSecurityFilter;
import com.betting.storage.util.HashingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import java.util.List;


@Configuration
@EnableWebSecurity
@Order(1)
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${com.betting.api.security.apikeytokenvalue}")
    private String apiKeyTokenValue;

    @Value("${com.betting.api.security.apikeysecretvalue}")
    private String apiKeySignatureSecretValue;
    @Value("#{'${com.betting.api.security.management.allowedips}'.split(';')}")
    private List<String> managementAllowedIps;

    @Autowired
    private final HashingService hashingService;

    public ApiSecurityConfig(HashingService hashingService) {
        this.hashingService = hashingService;
    }


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        ApiSecurityFilter filter = new ApiSecurityFilter(hashingService);
        filter.setAuthenticationManager(authentication -> {
            ApiSecurityFilter.AuthResult principal = (ApiSecurityFilter.AuthResult) authentication.getPrincipal();
            boolean auth = filter.evaluate(principal, apiKeyTokenValue, apiKeySignatureSecretValue, managementAllowedIps);
            if (!auth) {
                throw new BadCredentialsException("Auth failed");
            }
            authentication.setAuthenticated(true);
            return authentication;
        });
        httpSecurity
                .authorizeRequests()
//                    .antMatchers("/**")
//                    .permitAll()
                .anyRequest().authenticated().and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilter(filter);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers( "/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/security", "/swagger-ui.html", "/webjars/**", "/css/**", "/js/**");
    }

}
