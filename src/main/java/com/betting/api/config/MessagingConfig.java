package com.betting.api.config;

import com.betting.api.service.suggestion.SuggestionService;
import com.betting.storage.data.messaging.BettingMessageType;
import com.betting.storage.service.messaging.MessagingSubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@Slf4j
public class MessagingConfig {

    private final MessagingSubscriptionService messagingSubscriptionService;
    private final SuggestionService suggestionService;

    @Autowired
    public MessagingConfig(MessagingSubscriptionService messagingSubscriptionService, SuggestionService suggestionService) {
        this.messagingSubscriptionService = messagingSubscriptionService;
        this.suggestionService = suggestionService;
    }

    @PostConstruct
    public void init() {
        log.info("Subscribing to {}", BettingMessageType.NEW_SUGGESTION);
        messagingSubscriptionService
                .subscribe(BettingMessageType.NEW_SUGGESTION, m -> suggestionService.evict());
    }
}
