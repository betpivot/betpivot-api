package com.betting.api.monitoring;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

@Component
public class MdcInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //Collections.list(request.getHeaderNames()).forEach(hn -> MDC.put("http_header_" + hn, request.getHeader(hn)));
        MDC.put("path", request.getServletPath());
        return super.preHandle(request, response, handler);
    }
}
