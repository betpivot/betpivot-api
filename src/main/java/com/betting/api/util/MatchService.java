package com.betting.api.util;

import com.betting.api.model.prediction.RestPredictionMatchState;
import com.betting.api.model.suggestion.RestSuggestionMatchState;
import com.betting.api.model.suggestion.RestSuggestionState;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

@Service
public class MatchService {

    public RestSuggestionMatchState resolveSuggestionState(LocalDateTime localMatchStart, List<RestSuggestionState> states) {
        if (isFinished(localMatchStart)) {
            return RestSuggestionMatchState.FINISHED;
        }
        if (isPlaying(localMatchStart)) {
            return RestSuggestionMatchState.PLAYING;
        }

       return states.stream()
                .min(Comparator.comparing(s -> s.order))
                .orElse(RestSuggestionState.WAITING_FOR_PROVIDER).toMatchSuggestionState();
    }

    public RestPredictionMatchState resolvePredictionState(LocalDateTime localMatchStart) {
        if (isFinished(localMatchStart)) {
            return RestPredictionMatchState.FINISHED;
        }
        if (isPlaying(localMatchStart)) {
            return RestPredictionMatchState.PLAYING;
        }

        return RestPredictionMatchState.ACTIVE;
    }

    public boolean isPlaying(LocalDateTime localMatchStart) {
        if (localMatchStart.isBefore(ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime())) {
            return !isFinished(localMatchStart);
        }
        return Boolean.FALSE;
    }

    public boolean isFinished(LocalDateTime localMatchStart) {
        return  localMatchStart
                .isBefore(ZonedDateTime.now(ZoneOffset.UTC)
                        .minus(2, ChronoUnit.HOURS)
                        .minus(20, ChronoUnit.MINUTES).toLocalDateTime());
    }
}
