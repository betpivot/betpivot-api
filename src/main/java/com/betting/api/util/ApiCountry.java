package com.betting.api.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.ZoneId;
import java.time.ZoneOffset;

@Getter
@AllArgsConstructor
public enum ApiCountry {
    CZ(ZoneId.of("Europe/Prague"));

    private final ZoneId zone;
}
