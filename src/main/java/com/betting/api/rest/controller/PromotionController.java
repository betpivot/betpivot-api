package com.betting.api.rest.controller;

import com.betting.api.model.promotion.RestClientPromotion;
import com.betting.api.model.promotion.RestPromotion;
import com.betting.api.rest.response.FailResponse;
import com.betting.api.rest.response.OkResponse;
import com.betting.api.rest.response.Response;
import com.betting.api.service.promotion.performance.PromotionService;
import com.betting.storage.fail.ClientMessage;
import com.betting.storage.fail.FailException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/{country}/api/latest/user")
@Api(value="Promo codes operations", description="Operations to manage promo codes")
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    @RequestMapping(value = "/promotion/validatePromoCode/{promoCodeValue}", method = RequestMethod.GET)
    @ApiOperation(value = "Validate promo code", notes = "Validates promo code and returns object representing the promo code or FAIL\n" +
            "  * ~ state\n" +
            "    * ENABLED // Only this is returned\n" +
            "    * DISABLED // Not returned ATM")
    public OkResponse<RestPromotion> validatePromoCode(@PathVariable String promoCodeValue) {
        return Response.ok(promotionService.validatePromoCode(promoCodeValue));
    }

    @RequestMapping(value = "/promotion/promotions", method = RequestMethod.GET)
    @ApiOperation(value = "Get all promo codes", notes = "~ state\n" +
            "    * ENABLED // Only this is returned\n" +
            "    * DISABLED // Not returned ATM")
    public OkResponse<List<RestPromotion>> getAllActivePromotions() {
        return Response.ok(promotionService.getAllActivePromotions());
    }

    @RequestMapping(value = "/{clientId}/promotion", method = RequestMethod.POST)
    @ApiOperation(value = "Create and return promotion", notes = "~ state\n" +
            "    * ACTIVE //promo code is in active period\n" +
            "    * INACTIVE //promo code is not in active period\n" +
            "    * DISABLED //is not returned ATM" +
            "If promotion already exists return it")
    public OkResponse<RestClientPromotion> getOrCreateClientPromotion(@PathVariable String clientId) {
        return Response.ok(promotionService.getOrCreateClientPromotion(clientId));
    }

    @RequestMapping(value = "/{clientId}/promotion/{promoCodeValue}/activate", method = RequestMethod.POST)
    @ApiOperation(value = "Activate client promo code", notes = "~ state\n" +
            "    * ACTIVE //promo code is in active period\n" +
            "    * INACTIVE //promo code is not in active period\n" +
            "    * DISABLED //is not returned ATM")
    public OkResponse<RestClientPromotion> activateClientPromotion(@PathVariable String clientId, @PathVariable String promoCodeValue) {
        RestClientPromotion promo = promotionService.activateClientPromotion(clientId, promoCodeValue);
        promotionService.evictClientPromo(promo.clientId);
        return Response.ok(promo);
    }

    @ExceptionHandler(FailException.class)
    public FailResponse handleException(FailException e) {
        return FailResponse.fail(e);
    }

}
