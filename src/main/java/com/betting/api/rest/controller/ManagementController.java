package com.betting.api.rest.controller;

import com.betting.api.model.management.*;
import com.betting.api.rest.response.OkResponse;
import com.betting.storage.fail.FailException;
import com.betting.api.rest.response.FailResponse;
import com.betting.api.rest.response.Response;
import com.betting.api.service.management.ManagementService;
import com.betting.storage.service.push.PushNotificationType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/{country}/api/latest/management")
@Api(value="Management operations", description="Operations to manipulate with stored data")
public class ManagementController {


    @Autowired
    private ManagementService managementService;

    @RequestMapping(value = "/prediction", method = RequestMethod.POST)
    @ApiOperation(value = "Upload predictions", notes = "")
    public OkResponse<RestAddMatchResult> prediction(@RequestBody String body) {
        return Response.ok(managementService.addMatch(body));
    }

    @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
    @ApiOperation(value = "Upload prediction results", notes = "")
    public OkResponse<RestEvaluateMatchResult> evaluate(@RequestBody String body) {
        return Response.ok(managementService.evaluateMatch(body));
    }

    @ApiOperation(value = "Upload provider bets", notes = "")
    @RequestMapping(value = "/bets", method = RequestMethod.POST)
    public OkResponse<RestAddBetsResult> bets(@RequestBody List<RestAddBetsMatch> body) {
        return Response.ok(managementService.addDebugBets(body));
    }

    @ApiOperation(value = "Upload performance results", notes = "")
    @RequestMapping(value = "/performance", method = RequestMethod.POST)
    public OkResponse<RestAddPerformanceResult> bets(@RequestBody String body) {
        return Response.ok(managementService.addDebugPerformance(body));
    }

    @ApiOperation(value = "Send notification", notes = "")
    @RequestMapping(value = {"/notification/{type}/{providerName}", "/notification/{type}"}, method = RequestMethod.POST)
    public OkResponse<Boolean> notification(
            @PathVariable PushNotificationType type,
            @PathVariable(required = false) String providerName) {
        managementService.notification(type, providerName);
        return Response.ok(true);
    }

    @ApiOperation(value = "Invalidate cache", notes = "")
    @RequestMapping(value = {"/invalidateCache/{cacheName}"}, method = RequestMethod.POST)
    public OkResponse<RestInvalidateCacheResponse> invalidateCache(
            @PathVariable String cacheName) {
        return Response.ok(managementService.invalidateCache(cacheName));
    }

    @ExceptionHandler(FailException.class)
    public FailResponse handleException(FailException e) {
        return FailResponse.fail(e);
    }
}
