package com.betting.api.rest.controller;

import com.betting.storage.monitoring.Monitoring;
import io.swagger.annotations.Api;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.activation.FileTypeMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@RequestMapping("/{country}/api/latest/media/")
@Api(value="Media data", description="Operations to get media data")
public class MediaController {

    @GetMapping("/{resource}")
    public ResponseEntity<byte[]> getImage(
            @PathVariable String country,
            @PathVariable String resource,
            @RequestParam(name = "size", required = false) Integer size) {
        try {
            size = size == null ? 1 : size;
            String fileName = resource + "@" + size + "x.png";
            ClassPathResource image = new ClassPathResource("/public/images/" + resource + "/" + fileName);
            byte[] imageData = IOUtils.toByteArray(image.getInputStream());
            return ResponseEntity.ok()
                    .header("Content-Disposition", "attachment; filename=" + fileName)
                    .contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(fileName)))
                    .body(imageData);
        } catch (FileNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (IOException e) {
            Monitoring.serviceError("Exception when reading image", "api.media.image.fail.io", resource, getClass(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
