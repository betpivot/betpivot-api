package com.betting.api.rest.controller;

import com.betting.api.model.init.RestInitResponse;
import com.betting.api.rest.response.FailResponse;
import com.betting.api.rest.response.OkResponse;
import com.betting.api.rest.response.Response;
import com.betting.api.service.init.InitService;
import com.betting.storage.fail.BusinessFail;
import com.betting.storage.fail.ClientMessage;
import com.betting.storage.fail.FailException;
import com.betting.storage.fail.Severity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/{country}/api/latest")
@Api(value="Init operations", description="Operations that can be called without authorization")
public class InitController {

    private final InitService initService;

    @Autowired
    public InitController(InitService initService) {
        this.initService = initService;
    }

    @RequestMapping(value = "/init", method = RequestMethod.GET)
    @ApiOperation(value = "Returns init config", notes = "* ~state\n" +
            "    * RUNNING\n" +
            "    * NOT_RUNNING\n" +
            "\n" +
            " * ~providers[].{logo|icon}\n" +
            "    * Optional query param \"size\" can be used when retrieving image with values {1|2|3} defining image size. If not present size 1 is returned.")
    public OkResponse<RestInitResponse> init() {
        return Response.ok(initService.getInitConfig());
    }

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public OkResponse<String> ping() {
        return Response.ok("pong");
    }

    @ApiOperation(value = "Return all possible error messages")
    @RequestMapping(value = "/init/errors", method = RequestMethod.GET)
    public OkResponse<List<FailResponse>> errors() {
        Stream<FailException> clientMessage = Stream.of(ClientMessage.values()).map(v -> v.exception("client message detail"));
        Stream<FailException> businessMessages = Stream.of(BusinessFail.values()).map(v -> new FailException(Severity.INTERNAL_ERROR, v.getCode(), v.getMessage(), "business message detail"));
        return Response.ok(Stream.concat(clientMessage, businessMessages)
                .map(Response::fail)
                .collect(Collectors.toList()));
    }

    @ExceptionHandler(FailException.class)
    public FailResponse handleException(FailException e) {
        return FailResponse.fail(e);
    }
}
