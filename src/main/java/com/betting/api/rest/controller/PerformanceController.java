package com.betting.api.rest.controller;

import com.betting.api.model.performance.RestPerformancePeriod;
import com.betting.api.model.performance.RestPerformanceResultList;
import com.betting.api.rest.response.FailResponse;
import com.betting.api.rest.response.OkResponse;
import com.betting.api.rest.response.Response;
import com.betting.api.service.performance.PerformanceService;
import com.betting.storage.fail.FailException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/{country}/api/latest/performance/")
@Api(value="Performance data", description="Operations to get performance data")
public class PerformanceController {

    @Autowired
    private PerformanceService performanceService;

    @RequestMapping(value = "/periods", method = RequestMethod.GET)
    @ApiOperation(value = "Get performance periods", notes = "Used to generate graphs.")
    public OkResponse<List<RestPerformancePeriod>> getPeriods() {
        return Response.ok(performanceService.getPeriods());
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    @ApiOperation(value = "Get performance results", notes = "* ~result.resultValue\n" +
            "    * WINNING //Found winning result\n" +
            "    * NOT_WINNING //Did not find any winning result\n" +
            "    * WAITING_FOR_EVALUATION //His match is finished but results for the match has not been submitted\n" +
            "    * NOT_PLAYED // Player did not play\n" +
            "    * NOT_EVALUATED //Did not get evaluated (his match was evaluated but not him - error)")
    public OkResponse<RestPerformanceResultList> getResults() {
        return Response.ok(performanceService.getResults());
    }

    @ExceptionHandler(FailException.class)
    public FailResponse handleException(FailException e) {
        return FailResponse.fail(e);
    }

}
