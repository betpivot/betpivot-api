package com.betting.api.rest.controller;

import com.betting.api.model.prediction.RestPredictionMatch;
import com.betting.api.model.result.RestPredictionResult;
import com.betting.api.rest.response.FailResponse;
import com.betting.api.rest.response.OkResponse;
import com.betting.api.rest.response.Response;
import com.betting.api.service.prediction.PredictionService;
import com.betting.api.service.result.ResultService;
import com.betting.storage.fail.FailException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/{country}/api/latest/prediction")
@Api(value="Prediction data", description="Operations to get prediction data")
public class PredictionController {

    private final PredictionService predictionService;
    private final ResultService resultService;

    @Autowired
    public PredictionController(PredictionService predictionService, ResultService resultService) {
        this.predictionService = predictionService;
        this.resultService = resultService;
    }

    @RequestMapping(value = "/predictions", method = RequestMethod.GET)
    @ApiOperation(value = "Get active predictions", notes = " * ~state\n" +
            "    * ACTIVE //From prediction load to it's start time\n" +
            "    * PLAYING //Match is playing - no data returned\n" +
            "    * FINISHED //It is estimated match to be finished - no data returned\n" +
            "    * EVALUATED //We got results for this match - match in this state will not be present in API at this time\n" +
            "    \n" +
            " * ~team{1|2}.providers.predictionToSuggestionState\n" +
            "    * PROFITABLE // Atleast one active suggestion with PROFITABLE state exists\n" +
            "    * POTENTIALLY_PROFITABLE // No PROFITABLE active suggestion found but atleast on of these state found in active suggestions: WAITING_FOR_PROVIDER, PARSING_FAILED, NOT_PROFITABLE\n" +
            "    * MISSING // Only active suggestion in state MISSING_IN_PROVIDER found or no suggestion found at all.")
    public OkResponse<List<RestPredictionMatch>> getPredictions() {
        return Response.ok(predictionService.getPredictions());
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    @ApiOperation(value = "Get prediction results", notes = "")
    public OkResponse<List<RestPredictionResult>> getResults(@RequestParam Set<String> predictionToSuggestionIds) {
        return Response.ok(resultService.getByExternalIds(predictionToSuggestionIds));
    }

    @ExceptionHandler(FailException.class)
    public FailResponse handleException(FailException e) {
        return FailResponse.fail(e);
    }
}
