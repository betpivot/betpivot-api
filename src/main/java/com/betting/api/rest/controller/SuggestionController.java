package com.betting.api.rest.controller;

import com.betting.api.model.suggestion.RestSuggestionMatch;
import com.betting.api.rest.response.FailResponse;
import com.betting.api.rest.response.OkResponse;
import com.betting.api.rest.response.Response;
import com.betting.api.service.suggestion.SuggestionService;
import com.betting.storage.fail.FailException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/{country}/api/latest/suggestion")
@Api(value="Suggestion data", description="Operations to get suggestion data")
public class SuggestionController {

    @Autowired
    private SuggestionService suggestionService;

    @RequestMapping(value = "/suggestions", method = RequestMethod.GET)
    @ApiOperation(value = "Get all active suggestions", notes = "Enums:\n" +
            " * ~players.suggestions.type\n" +
            "    * LESS\n" +
            "    * MORE\n" +
            " * ~players.suggestions.state\n" +
            "    * WILL_NOT_PLAY // Will play prediction for this player is less then 90%\n" +
            "    * PROFITABLE //We have data provider and it is profitable to bet on \n" +
            "    * NOT_PROFITABLE //We have provider data but it is not profitable to bet on\n" +
            "    * WAITING_FOR_PROVIDER //Match is not in provider\n" +
            "    * MISSING_IN_PROVIDER //Match is in provider but player is missing\n" +
            "    * PARSING_FAILED //Technical error parsing match\n" +
            " * ~state\n" +
            "    * WAITING //Waiting for first bets by any provider\n" +
            "    * BETTING //Active prematch betting\n" +
            "    * BETTING_NO_SUGGESTIONS //Data from all providers loaded either found no profitable suggestions or bet for player is missing\n" +
            "    * PLAYING //Match is playing - no data returned\n" +
            "    * FINISHED //It is estimated match to be finished - no data returned\n" +
            "    * EVALUATED //We got results for this match - match in this state will not be present in API at this time")
    public OkResponse<List<RestSuggestionMatch>> getSuggestions(
            @RequestParam(value="providers", required = false) String[] providers) {
        return Response.ok(suggestionService.getActiveSuggestions(providers == null ? Collections.emptyList() : Arrays.asList(providers)));
    }

    @ExceptionHandler(FailException.class)
    public FailResponse handleException(FailException e) {
        return FailResponse.fail(e);
    }
}
