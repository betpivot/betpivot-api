package com.betting.api.rest.response;

import com.betting.storage.fail.Fail;

public class Response {
    public final Status status;

    public Response(Status status) {
        this.status = status;
    }

    public static <T> OkResponse<T> ok(T data) {
        return new OkResponse<>(Status.OK, data);
    }

    public static FailResponse fail(Fail data) {
        Status status = null;
        switch (data.severity()) {
            case INTERNAL_ERROR: status = Status.FAIL;
                break;
            case VALIDATION: status = Status.VALIDATION_FAIL;
                break;
        }

        return new FailResponse(status, data.code(), data.message(), data.detailMessage());
    }

    public enum Status {
        OK(200),
        FAIL(500),
        VALIDATION_FAIL(400);

        public final int code;

        Status(int code) {
            this.code = code;
        }
    }
}
