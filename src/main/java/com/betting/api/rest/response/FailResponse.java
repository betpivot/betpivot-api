package com.betting.api.rest.response;

public class FailResponse extends Response {
    public final int code;
    public final String message;
    public final String detailMessage;

    public FailResponse(Status status, int code, String message, String detailMessage) {
        super(status);
        this.code = code;
        this.message = message;
        this.detailMessage = detailMessage;
    }
}
