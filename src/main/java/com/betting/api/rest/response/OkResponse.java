package com.betting.api.rest.response;

public class OkResponse<T> extends Response {

    public final T data;

    public OkResponse(Status status, T data) {
        super(status);
        this.data = data;
    }
}
