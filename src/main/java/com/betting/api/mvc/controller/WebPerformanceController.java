package com.betting.api.mvc.controller;

import com.betting.api.service.performance.PerformanceService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class WebPerformanceController {

    private final PerformanceService performanceService;

    @Autowired
    public WebPerformanceController(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    @GetMapping("/public/vysledky")
    public String results(Model model) {
        model.addAttribute("results", performanceService.getAllResults());
        return "results";
    }

    @GetMapping("/public/vysledky/csv")
    public void results(HttpServletResponse response) throws IOException {
        response.setContentType ("application/csv");
        response.setHeader ("Content-Disposition", "attachment; filename=\"betpivot_vysledky_" + System.currentTimeMillis() + ".csv\"");
        try (PrintWriter writer = response.getWriter()) {
            try (CSVPrinter csvPrinter = new CSVPrinter(new BufferedWriter(writer), CSVFormat.DEFAULT.withRecordSeparator("\n"))) {
                csvPrinter.printRecord("Sazkovka", "Jmeno hrace", "Tym", "Typ", "Kurz", "Linie", "Pocet bodu", "Vysledek", "Cas vsazeni");
                List<Object[]> stream = performanceService.getAllResults().stream()
                        .map(r -> new Object[]{r.providerName, r.playerName, r.teamNameId, r.type, r.rate, r.line, r.result, r.resultValue, r.betImportTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"))})
                        .collect(Collectors.toList());
                csvPrinter.printRecords(stream);
            }
        }
    }

}
