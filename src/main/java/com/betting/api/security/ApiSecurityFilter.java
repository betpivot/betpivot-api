package com.betting.api.security;

import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.util.HashingService;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;

public class ApiSecurityFilter extends AbstractPreAuthenticatedProcessingFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiSecurityFilter.class);

    private static final String HEADER_TOKEN = "BetpivotApiToken";
    private static final String HEADER_TIMESTAMP = "BetpivotApiTimestamp";
    private static final String HEADER_SIGNATURE = "BetpivotApiSignature";

    public static final int TIMESTAMP_TTL = 10;

    private final HashingService hashingService;

    public ApiSecurityFilter(HashingService hashingService) {
        this.hashingService = hashingService;
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        String token = request.getHeader(HEADER_TOKEN);
        String timestamp = request.getHeader(HEADER_TIMESTAMP);
        String signature = request.getHeader(HEADER_SIGNATURE);
        String[] urlParts = request.getServletPath().split("/");
        ApiSecurityProtectedResource resource = urlParts.length < 5 ? null : ApiSecurityProtectedResource.resolvePath(urlParts[4]);
        String loadBalancedIp = request.getHeader("X-Forwarded-For");
        if (loadBalancedIp != null && !loadBalancedIp.isEmpty()) {
            loadBalancedIp = loadBalancedIp.split(", ")[0];
        }
        String remoteAddr = loadBalancedIp == null || loadBalancedIp.isEmpty() ? request.getRemoteAddr() : loadBalancedIp;
        AuthResult auth = new AuthResult(token, timestamp, signature, resource, remoteAddr);
        LOGGER.info("Principal {}", auth);
        return auth;
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return "N/A";
    }

    public boolean evaluate(AuthResult auth, String token, String secret, List<String> allowedIps) {
        if (auth.resource == null) {
            Monitoring.serviceInfo("Auth failed", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }
        if (ApiSecurityProtectedResource.NO_AUTH.equals(auth.resource)) {
            return true;
        }
        if (ApiSecurityProtectedResource.IP_AUTH.equals(auth.resource) && !allowedIps.contains(auth.ip)) {
            Monitoring.serviceError("Auth failed: IP not allowed", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }
        if (auth.token == null || auth.timestamp == null || auth.signature == null || auth.rawTimestamp == null) {
            Monitoring.serviceWarning("Auth failed: param missing", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }
        String signatureCheck = hashingService.hash(auth.token + auth.rawTimestamp + secret);
        if (!signatureCheck.equalsIgnoreCase(auth.signature)) {
            Monitoring.serviceWarning("Auth failed: signature mismatch", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }
        if (!auth.token.equals(token)) {
            Monitoring.serviceWarning("Auth failed: token mismatch", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }
        LocalDateTime utcNow = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
        LocalDateTime ttlDateTime = utcNow
                .minus(TIMESTAMP_TTL, ChronoUnit.MINUTES);
        if (auth.timestamp.isBefore(ttlDateTime)) {
            Monitoring.serviceWarning("Auth failed: signature expired", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }

        if (auth.timestamp.isAfter(utcNow.plus(5, ChronoUnit.SECONDS))) {
            Monitoring.serviceWarning("Auth failed: signature in future (" + auth.timestamp +"/" + utcNow + ")", "api.security.auth.failed", auth.ip, getClass());
            return false;
        }

        return true;
    }

    @ToString
    public static class AuthResult {
        public final String token;
        public final LocalDateTime timestamp;
        public final Long rawTimestamp;
        public final String signature;
        public final ApiSecurityProtectedResource resource;
        public final String ip;

        public AuthResult(String token, String stringTimestamp, String signature, ApiSecurityProtectedResource resource, String ip) {
            this.token = token;
            this.rawTimestamp =resolveRawTimestamp(stringTimestamp);
            this.resource = resource;
            this.ip = ip;
            this.timestamp = resolveTimestamp(rawTimestamp);
            this.signature = signature;
        }

        private Long resolveRawTimestamp(String stringTimestamp) {
            try {
                return Long.valueOf(stringTimestamp);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        private LocalDateTime resolveTimestamp(Long rawTimestamp) {
            if (rawTimestamp == null) {
                return null;
            }
            try {
                return Instant
                        .ofEpochMilli(rawTimestamp)
                        .atZone(ZoneOffset.UTC).toLocalDateTime();
            } catch (Exception e) {
               return null;
            }
        }
    }
}