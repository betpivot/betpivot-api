package com.betting.api.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Getter
public enum ApiSecurityProtectedResource {
    IP_AUTH("management"),
    TOKEN_AUTH("prediction", "suggestion", "performance", "user"),
    NO_AUTH("init", "media");

    private final List<String> pathNames;

    ApiSecurityProtectedResource(String... pathNames) {
        this.pathNames = Arrays.asList(pathNames);
    }

    public static ApiSecurityProtectedResource resolvePath(String pathName) {
        return Arrays.stream(values())
                .filter(v -> v.pathNames.contains(pathName)).findFirst()
                .orElse(null);
    }
}
