package com.betting.api.model.suggestion;

public enum RestSuggestionMatchState {
    WAITING, //Waiting for first bets by any provider
    BETTING, //Active prematch betting
    BETTING_NO_SUGGESTIONS,
    PLAYING, //Match is playing - no data returned
    FINISHED, //It is estimated match to be finished - no data returned
    EVALUATED //We got results for this match - match in this state will not be present in API at this time
}
