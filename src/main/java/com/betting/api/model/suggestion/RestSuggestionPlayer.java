package com.betting.api.model.suggestion;

import lombok.AllArgsConstructor;

import java.util.List;
@AllArgsConstructor
public class RestSuggestionPlayer {

    public final String name;
    public final String predictionToSuggestionId;

    public final List<RestSuggestion> suggestions;
}
