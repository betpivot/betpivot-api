package com.betting.api.model.suggestion;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;

import java.time.ZonedDateTime;
import java.util.List;

@AllArgsConstructor
public class RestSuggestionMatch {

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime startTime;
    public final RestSuggestionMatchState state;

    public final RestSuggestionTeam team1;
    public final RestSuggestionTeam team2;
}
