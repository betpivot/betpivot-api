package com.betting.api.model.suggestion;

public enum RestSuggestionType {

    MORE,
    LESS;
}
