package com.betting.api.model.suggestion;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class RestSuggestionTeam {
    public final String nameId;
    public final List<RestSuggestionPlayer> players;
}
