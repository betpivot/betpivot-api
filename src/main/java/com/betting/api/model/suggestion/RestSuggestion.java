package com.betting.api.model.suggestion;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestSuggestion {

    public final String providerId;
    public final RestSuggestionType type;
    public final Double dontBetUnder;
    public final Double rate;
    public final Double line;
    public final String betLink;
    public final String betDeepLink;
    public final RestSuggestionState state;
    public final Double probability;

}
