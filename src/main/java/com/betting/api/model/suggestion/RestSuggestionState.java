package com.betting.api.model.suggestion;

public enum RestSuggestionState {
    WILL_NOT_PLAY(0),
    PROFITABLE(10),
    WAITING_FOR_PROVIDER(20),
    PARSING_FAILED(30),
    NOT_PROFITABLE(40),
    MISSING_IN_PROVIDER(50);

    public final int order;

    RestSuggestionState(int order) {
        this.order = order;
    }

    public RestSuggestionMatchState toMatchSuggestionState() {
        switch (this) {
            case PROFITABLE:  return RestSuggestionMatchState.BETTING;
            case PARSING_FAILED:
            case WAITING_FOR_PROVIDER: return RestSuggestionMatchState.WAITING;
            default: return RestSuggestionMatchState.BETTING_NO_SUGGESTIONS;
        }
    }
}
