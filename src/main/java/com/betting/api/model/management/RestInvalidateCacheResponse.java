package com.betting.api.model.management;

import lombok.Data;

@Data
public class RestInvalidateCacheResponse {
    public final String cache;
}
