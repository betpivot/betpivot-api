package com.betting.api.model.management;

import lombok.Data;

@Data
public class RestAddPerformanceResult {

    private final int numberOfResults;
}
