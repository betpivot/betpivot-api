package com.betting.api.model.management;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestAddBetsPlayerName {
    private String firstName;
    private String lastName;

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }
}
