package com.betting.api.model.management;

import lombok.Data;

@Data
public class RestAddMatchResult {

    public final Integer numberOfAddedMatches;
    public final Integer numberOfAddedPredictions;
    public final Integer numberOfRetiredMatches;
    public final boolean notificationSend;
}
