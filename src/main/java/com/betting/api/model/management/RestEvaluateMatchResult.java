package com.betting.api.model.management;

import lombok.Data;

@Data
public class RestEvaluateMatchResult {

    public final Long numberOfEvaluatedMatches;
    public final Long numberOfEvaluatedSuggestionMatches;

    public final Integer numberOfResults;

    public final boolean sendNotification;
}
