package com.betting.api.model.management;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.mongodb.morphia.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestAddBetsMatch {

    private String team1;

    private String team2;

    private String externalMatchId;

    private String providerName;

    private List<RestAddBetsBet> team1Bets = new ArrayList<>();

    private List<RestAddBetsBet> team2Bets = new ArrayList<>();
}
