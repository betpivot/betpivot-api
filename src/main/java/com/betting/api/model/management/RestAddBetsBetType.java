package com.betting.api.model.management;

public enum RestAddBetsBetType {
    MORE,
    LESS;

    public boolean more() {
        return MORE.equals(this);
    }

    public boolean less() {
        return LESS.equals(this);
    }
}
