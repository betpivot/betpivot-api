package com.betting.api.model.management;

import lombok.Data;

@Data
public class RestAddBetsResult {
    public final Long numberOfBetsAdded;
    public final Long numberOfMatchesAdded;
}
