package com.betting.api.model.management;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestAddBetsBet {

    private RestAddBetsPlayerName playerName;

    private Double line;
    private RestAddBetsBetType type;

    private Double rate;

    private String betLinkUrl;
}
