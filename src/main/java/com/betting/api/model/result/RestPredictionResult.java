package com.betting.api.model.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class RestPredictionResult {
    public final Integer points;
    public final Boolean played;
    public final String predictionToSuggestionId;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime importTime;
}
