package com.betting.api.model.promotion;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class RestPromotion {

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime start;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime end;

    public final RestPromotionState state;
}
