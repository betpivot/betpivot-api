package com.betting.api.model.promotion;

public enum RestClientPromotionState {
    ACTIVE,
    INACTIVE,
    DISABLED;
}
