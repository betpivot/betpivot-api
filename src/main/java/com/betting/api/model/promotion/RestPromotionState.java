package com.betting.api.model.promotion;

public enum RestPromotionState {
    ENABLED,
    DISABLED;
}
