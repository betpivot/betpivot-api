package com.betting.api.model.init;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class RestTeam {
    public final String nameId;
    public final String name;
}
