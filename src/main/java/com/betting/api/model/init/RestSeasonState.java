package com.betting.api.model.init;

public enum RestSeasonState {
    RUNNING,
    NOT_RUNNING;
}
