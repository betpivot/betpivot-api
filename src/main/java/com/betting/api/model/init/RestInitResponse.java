package com.betting.api.model.init;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class RestInitResponse {

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime serverTime;

    public final RestSeason season;
    public final RestLocalization localization;
    public final String consentUrl;
    public final String unlockAdUnitId;

    public final List<RestBettingProvider> providers;
    public final List<RestTeam> teams;
}
