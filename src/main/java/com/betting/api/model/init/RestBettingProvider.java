package com.betting.api.model.init;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestBettingProvider {
    public final String id;
    public final String name;
    public final String logo;
    public final String icon;
    public final String color;
    public final String notificationTopic;
    public final String registrationUrl;
}
