package com.betting.api.model.init;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestLocalization {
    public final String teamJoinCharacter;
    public final RestTeamOrder teamOrder;
}
