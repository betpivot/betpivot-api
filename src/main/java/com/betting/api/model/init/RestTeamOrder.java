package com.betting.api.model.init;

public enum RestTeamOrder {
    TEAM1_FIRST,
    TEAM2_FIRST;
}
