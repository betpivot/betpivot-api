package com.betting.api.model.performance;

public enum RestPerformanceResultValue {
    WINNING,
    NOT_WINNING,
    NOT_PLAYED,
    WAITING_FOR_EVALUATION,
    NOT_EVALUATED;
}
