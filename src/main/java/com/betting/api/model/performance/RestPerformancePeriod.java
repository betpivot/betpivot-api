package com.betting.api.model.performance;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class RestPerformancePeriod {
    public final RestPerformancePeriodTitle title;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime start;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime end;

    public final Double roiWithOptimalRate;
    public final Double roiWithDontBetUnderRate;
    public final Integer resultsCount;

    public final RestPerformanceGraph graph;
}
