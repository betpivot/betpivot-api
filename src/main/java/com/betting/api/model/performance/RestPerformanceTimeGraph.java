package com.betting.api.model.performance;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RestPerformanceTimeGraph {
    public final String firstDataPointValue;
    public final String secondDataPointValue;
    public final List<List<Double>> dataPoints;
}
