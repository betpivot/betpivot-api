package com.betting.api.model.performance;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class RestPerformanceResultList {
    public final List<RestPerformanceResult> results;
}
