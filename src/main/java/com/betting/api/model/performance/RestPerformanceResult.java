package com.betting.api.model.performance;

import com.betting.api.model.suggestion.RestSuggestionType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import org.springframework.data.annotation.Transient;

import java.time.ZonedDateTime;

@Data
public class RestPerformanceResult {

    public final String playerName;
    public final String teamNameId;
    public final String providerId;
    public final String providerName;
    public final RestSuggestionType type;
    public final Double dontBetUnder;
    public final Double rate;
    public final Double line;

    public final String predictionToSuggestionId;

    public final Integer result;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime betImportTime;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime predictionEvaluationTime;

    public final RestPerformanceResultValue resultValue;
}
