package com.betting.api.model.performance;

import lombok.Getter;

import java.time.*;
import java.util.function.BiFunction;
import java.util.function.Function;

@Getter
public enum RestPerformancePeriodTitle {
    PERFORMANCE_LAST_MONTH((s, n) -> n.minus(Period.ofMonths(1))),
    PERFORMANCE_LAST_THREE_MONTH((s, n) -> n.minus(Period.ofMonths(3))),
    PERFORMANCE_THIS_SEASON((s, n) -> s),
    PERFORMANCE_ALL((s, n) -> LocalDateTime.of(2017, 10, 24, 0, 0));

    private final BiFunction<LocalDateTime, LocalDateTime, LocalDateTime> periodResolver;

    RestPerformancePeriodTitle(BiFunction<LocalDateTime, LocalDateTime, LocalDateTime> periodResolver) {
        this.periodResolver = periodResolver;
    }
    public LocalDateTime resolvePeriodStart(LocalDateTime seasonStart) {

        return periodResolver.apply(seasonStart, LocalDateTime.now());
    }

}
