package com.betting.api.model.performance;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class RestPerformanceGraph {
    public final Integer dataPointsCount;
    public final Integer resultsInDataPoint;
    public final List<Double> dataPoints;
}
