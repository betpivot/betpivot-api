package com.betting.api.model.prediction;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RestPredictionPlayer {

    public final String name;
    public final String predictionToSuggestionId;
    public final PredictionToSuggestionState predictionToSuggestionState;

    public final RestPrediction prediction;
}
