package com.betting.api.model.prediction;

import com.betting.api.model.suggestion.RestSuggestionMatchState;
import com.betting.api.model.suggestion.RestSuggestionPlayer;
import com.betting.api.model.suggestion.RestSuggestionTeam;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;

import java.time.ZonedDateTime;
import java.util.List;

@AllArgsConstructor
public class RestPredictionMatch {

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ")
    public final ZonedDateTime startTime;

    public final RestPredictionMatchState state;

    public final RestPredictionTeam team1;
    public final RestPredictionTeam team2;
}
