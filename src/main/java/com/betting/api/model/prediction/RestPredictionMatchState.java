package com.betting.api.model.prediction;

public enum RestPredictionMatchState {
    ACTIVE,
    PLAYING,
    FINISHED,
    EVALUATED;
}
