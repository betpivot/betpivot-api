package com.betting.api.model.prediction;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RestPrediction {
    public final Double willPlay;
    public final Double playTime;
    public final Double points;
}
