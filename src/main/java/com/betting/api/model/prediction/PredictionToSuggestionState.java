package com.betting.api.model.prediction;

import com.betting.storage.data.suggestion.SuggestionState;

public enum PredictionToSuggestionState {
    PROFITABLE,
    POTENTIALLY_PROFITABLE,
    MISSING;

    public static PredictionToSuggestionState resolve(SuggestionState s) {
        switch (s) {
            case PROFITABLE: return PROFITABLE;
            case MISSING_IN_PROVIDER: return MISSING;
            default: return POTENTIALLY_PROFITABLE;
        }
    }
}
