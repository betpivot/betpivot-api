package com.betting.api.model.prediction;

import com.betting.api.model.suggestion.RestSuggestionPlayer;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class RestPredictionTeam {
    public final String nameId;
    public final List<RestPredictionPlayer> players;
}
